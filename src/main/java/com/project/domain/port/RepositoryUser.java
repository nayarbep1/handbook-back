package com.project.domain.port;

import com.project.domain.model.ModelUser;
import org.springframework.stereotype.Repository;

@Repository
public interface RepositoryUser {

    ModelUser createUser(ModelUser modelUser);
}
