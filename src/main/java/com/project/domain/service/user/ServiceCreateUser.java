package com.project.domain.service.user;

import com.project.domain.model.ModelUser;
import com.project.domain.port.RepositoryUser;

public class ServiceCreateUser {

    private RepositoryUser repositoryUser;

    public ServiceCreateUser(RepositoryUser repositoryUser) {
        this.repositoryUser = repositoryUser;
    }

    public ModelUser run(ModelUser modelUser){
        return this.repositoryUser.createUser(modelUser);
    }
}
