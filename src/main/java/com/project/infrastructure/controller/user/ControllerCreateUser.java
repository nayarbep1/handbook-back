package com.project.infrastructure.controller.user;

import com.project.application.handler.command.CommandUser;
import com.project.application.handler.user.HandlerCreateUser;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
public class ControllerCreateUser {

    private HandlerCreateUser handlerCreateUser;

    public ControllerCreateUser(HandlerCreateUser handlerCreateUser) {
        this.handlerCreateUser = handlerCreateUser;
    }

    @PostMapping(value = "/create")
    public void run(@RequestBody CommandUser commandUser){
        this.handlerCreateUser.run(commandUser);
    }
}
