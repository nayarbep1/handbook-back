package com.project.infrastructure.adapter.repository.mapper;

import com.project.domain.model.ModelUser;
import com.project.infrastructure.entity.EntityUser;

public class MapperUser {

    public MapperUser() {
    }

    public EntityUser modelToEntity(ModelUser modelUser){
        return new EntityUser(modelUser.getId(), modelUser.getName(), modelUser.getLastName(), modelUser.getEmail(), modelUser.getPassword());
    }

    public ModelUser entityToModel(EntityUser entityUser){
        return new ModelUser(entityUser.getId(), entityUser.getName(), entityUser.getLastName(), entityUser.getEmail(), entityUser.getPassword());
    }
}
