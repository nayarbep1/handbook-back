package com.project.infrastructure.adapter.repository.user;

import com.project.domain.model.ModelUser;
import com.project.domain.port.RepositoryUser;
import com.project.infrastructure.adapter.repository.mapper.MapperUser;
import com.project.infrastructure.entity.EntityUser;
import com.project.infrastructure.jparepository.JpaUserRepository;
import org.springframework.stereotype.Repository;

@Repository
public class RepositoryCreateUserImpl implements RepositoryUser {

    private JpaUserRepository jpaUserRepository;
    private MapperUser mapperUser = new MapperUser();

    public RepositoryCreateUserImpl(JpaUserRepository jpaUserRepository) {
        this.jpaUserRepository = jpaUserRepository;
    }

    public ModelUser createUser(ModelUser modelUser){
        EntityUser entityUser = this.mapperUser.modelToEntity(modelUser);
        EntityUser entityUserSaved = this.jpaUserRepository.save(entityUser);
        return this.mapperUser.entityToModel(entityUserSaved);
    }

}
