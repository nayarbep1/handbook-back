package com.project.infrastructure.configuration;

import com.project.domain.port.RepositoryUser;
import com.project.domain.service.user.ServiceCreateUser;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanServiceUser {

    @Bean
    public ServiceCreateUser serviceCreateUser(RepositoryUser repositoryUser){
        return new ServiceCreateUser(repositoryUser);
    }
}
