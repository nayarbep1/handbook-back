package com.project.application.handler.user;

import com.project.application.factory.FactoryUser;
import com.project.application.handler.command.CommandUser;
import com.project.domain.model.ModelUser;
import com.project.domain.service.user.ServiceCreateUser;
import org.springframework.stereotype.Component;

@Component
public class HandlerCreateUser {

    private final ServiceCreateUser serviceCreateUser;
    private final FactoryUser factoryUser;

    public HandlerCreateUser(ServiceCreateUser serviceCreateUser, FactoryUser factoryUser){
        this.serviceCreateUser = serviceCreateUser;
        this.factoryUser = factoryUser;
    }

    public ModelUser run(CommandUser commandUser){
        return this.serviceCreateUser.run(this.factoryUser.createUser(commandUser));
    }
}
