package com.project.application.factory;

import com.project.application.handler.command.CommandUser;
import com.project.domain.model.ModelUser;
import org.springframework.stereotype.Component;

@Component
public class FactoryUser {

    public ModelUser createUser(CommandUser commandUser){
        return new ModelUser(commandUser.getName(), commandUser.getLastName(), commandUser.getEmail(), commandUser.getPassword());
    }

}
